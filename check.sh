export PATH="/home/arnaud/.local/bin/:$PATH" && \
restic --verbose -r rclone:gdrive-backup:restic \
    --password-file=$HOME/restic/cred-gdrive.txt check \
    --read-data-subset=1.0%