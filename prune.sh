export PATH="/home/arnaud/.local/bin/:$PATH" && \
notify-send --urgency=low 'Restic' 'Started pruning Gdrive repo (should take a few minutes).' && \
restic prune -r rclone:gdrive-backup:restic \
	 --password-file $DRIVE_PASSWORD_FILE && \
restic check -r rclone:gdrive-backup:restic \
	 --password-file $DRIVE_PASSWORD_FILE
