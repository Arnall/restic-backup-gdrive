# Scheduled restic backups on Google Drive

From [Fedora Magazine article](https://fedoramagazine.org/automate-backups-with-restic-and-systemd/)

- Creates snapshots every day
- Forgets snapshots based on a policy (see restic-backup.conf)
- Weekly prunes (actually cleans up data) old snapshots and checks repo integrity
- Desktop notifications using notify-send
- All services will wait for a network connection to start
- Missed scheduled operations are kept to be executed later (unlike a cron job)
- No backup on weekends
- Easy to manually trigger a new backup at anytime

# Installation

- All unit files (`.service` and `.timer` extensions) and bash scripts (`.sh`) are to be put in `~/.config/systemd/user`
- `restic-backup.conf` is located in `~/.config/`
- `restic` and `rclone` should be available system-wide or in `$PATH` (see bash scripts)
- A restic repo should be created beforehead at the desired location. Here, it is `rclone:gdrive-backup:restic`
- restic secrets and exclude directives are in ~/restic (see `restic-backup.conf`). Don't `git add` your secrets !
- restic forget policy is set in `restic-backup.conf`
- Backup, prune and check frequency are set in `.timer` files

I downloaded restic from the Github repo (Amd64 version).

# Automated use

## Start services

Running `systemctl --user daemon-reload` is necessary after changing unit files. Then :

```shell
systemctl --user enable --now restic-backup.timer
systemctl --user enable --now restic-prune.timer
```

## Monitor

When services are done or fail, desktop notifications are sent. 
To check everything is doing fine, or read the logs in case of a failure :

The schedulers :

```shell
systemctl --user status restic-backup.timer
systemctl --user status restic-prune.timer
```

The services itself :

```shell
systemctl --user status restic-backup.service
systemctl --user status restic-prune.service
```

## Stop

Suspend until next reboot :

```shell
systemctl --user stop restic-backup.timer
systemctl --user stop restic-prune.timer
```

Disable it for future startups (timer will not start on boot, but can keep running now) :

```
systemctl --user disable restic-backup.timer
systemctl --user disable restic-prune.timer
```

# Manual use

## Force a backup now

```shell
systemctl --user start restic-backup.service
```

## Force a prune + check now

```shell
systemctl --user start restic-prune.service
```

## Refresh a Gdrive token

```shell
rclone config update gdrive-backup config_refresh_token true
```

## Unlock the repo

If an operation stopped unexpectedly, the repo can sometimes stay locked.

```shell
restic -r rclone:gdrive-backup:restic --password-file ~/restic/cred-gdrive.txt unlock
```

# Restoring files

## Mount the repo

The repo can be easily explored by mounting it with FUSE :

```shell
mkdir -p /tmp/restic
restic -r rclone:gdrive-backup:restic --password-file ~/restic/cred-gdrive.txt mount /tmp/restic
cd /tmp/restic/snapshots
```
